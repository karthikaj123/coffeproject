package main;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

//menu class
public class Menu {

	public static void main(String[] args) throws Exception {

		FileReader reader = new FileReader("src/config.properties");

		Properties properties = new Properties();
		properties.load(reader);

		Scanner scanner = new Scanner(System.in);
		int ch;
//				CoffeeService coffeeService=new CoffeeServiceArrayImpl();
//			CoffeeService coffeeService=new CoffeeServiceArrayListImpl();
	CoffeeService coffeeService = CoffeeServiceFactory.getCoffeeService("Array", properties);
		do {
			System.out.println(
					"1.Add\n2.Display\n3.Search\n4.Sort\n5.SearchDate\n6.delete\n7.download csv\n8.downlod json\n9.Exit\nEnter the choice:");
			ch = scanner.nextInt();
			switch (ch) {

//get the input from the user and handling the exception
			case 1:
				try {
					System.out.println("enter the coffee name:");
					String n = scanner.next();
					System.out.println("enter the brand name:");
					String b = scanner.next();
					System.out.println("enter the price");
					int p = scanner.nextInt();
					System.out.println("Enter the order date :");
					String date = scanner.next();

					LocalDate d = LocalDate.parse(date);
					System.out.println("Enter the type of coffee");

					String typevalue = scanner.next();
					CoffeeType type = CoffeeType.valueOf(typevalue.toUpperCase());
					Coffee coffee = new Coffee(n, b, p, d, type);
					coffeeService.add(coffee);
				}
				catch (Exception exception) {
					System.out.println("Exception");
				}
				break;

			case 2:
//				display the input taken from the user
				coffeeService.displayElements();
				break;
			case 3:
//						Search the elements using name of the coffee and print the particular coffee details
				System.out.println("serach element:");
				String name = scanner.next();
				coffeeService.search(name);
				break;
			case 4:
//						sort the coffees based on price values
				coffeeService.sort();
				break;
			case 5:
//						Search the coffees based on date
				System.out.println("Enter the date:");
				String checkDate = scanner.next();
				LocalDate d = LocalDate.parse(checkDate);
				List<Coffee>coffees=coffeeService.search(d);
				coffeeService.search(d);
				break;

			case 6:
				
				System.out.println("Enter element to delete");
				String name1=scanner.next();
				coffeeService.delete(name1);

				break;
			case 7:
			{
				try {
					coffeeService.download();
				}

				catch (Exception e) {
					e.printStackTrace();

				}
			}
				break;

			case 8:

				try {

					coffeeService.downloadJSON();
				} catch (Exception e) {
					e.printStackTrace();

				}
				break;

			case 9:
//						exit from the switch block 
				System.exit(0);
				break;
			default:
//						If no matches found,print default statement
				System.out.println("invalid");
				break;
			}
		} while (ch != 7);

		scanner.close();

	}

}
