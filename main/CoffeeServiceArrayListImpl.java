package main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class CoffeeServiceArrayListImpl implements CoffeeService {

	List<Coffee> CoffeeList=new ArrayList<>();

	
	/**
	 * The method will add coffee list
	 */

	@Override
	public void add(Coffee coffee) {
		if(CoffeeList.size()>0 && CoffeeList.contains(coffee)) {
			System.out.println("Already exist");
		
		}
		
		else {
		CoffeeList.add(coffee);
	}
	}
	
	/**
	 * The method will display coffees
	 */
	@Override
	public void displayElements() {
		CoffeeList.forEach(System.out::println);

	}
	
	/**
	 * The method will search for coffees
	 */

	@Override
	public void search(String name) {
		
		System.out.println(CoffeeList.stream().filter(coffee -> coffee.getname().equalsIgnoreCase(name))
			.collect(Collectors.toList()));
		
		
			}

	

	@Override
	public void sort() {
		Collections.sort(CoffeeList);
		displayElements();

	}

	@Override
	public List<Coffee> search(LocalDate date) {
		return CoffeeList.stream().filter(coffees -> coffees.getDate().isAfter(date)).collect(Collectors.toList());

	}

	@Override
	public void download() throws IOException {
		FileWriter file = new FileWriter("CoffeeOrderDetails.csv");
		int j = 0;
		for (int i = 0; i < j; i++) {
			String result = convertTocsv(CoffeeList.get(i));
			file.write(result);

		}
		file.close();
	}

	@Override
	public void downloadJSON() throws Exception {

		String data;
		FileWriter file = new FileWriter("playerDownload.Json");
		for (int i = 0; i < CoffeeList.size(); i++) {
			if (i == 0)
				data = "[" + getJSON(CoffeeList.get(i));
			else
				data = "," + getJSON(CoffeeList.get(i));
			if (i == CoffeeList.size() - 1)
				data = data + "]";
			file.write(data);
		}
		file.close();
	}

	

	public void delete(String name) {

		for (Iterator<Coffee> iter = CoffeeList.iterator(); iter.hasNext();) {
			Coffee coffee = iter.next();
			if (coffee.getname().equals(name)) {
				iter.remove();
			}
		}
	}
			
		

	}


