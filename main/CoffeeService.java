package main;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
//interface coffeetype

public interface CoffeeService {
	void add(Coffee coffee);
	void displayElements() throws Exception ;
	void search(String name) throws Exception ;
	void sort() throws Exception  ;
	 List<Coffee>search(LocalDate date) throws Exception;
    
    default String convertTocsv(Coffee coffee ) {
    	
		return new StringBuffer (coffee.getname()).append(",").append(coffee.getBrandname()).append(",")
				.append(coffee.getPrice()).append(", ")
				.append(coffee.getDate()).append(",").append(coffee.getType()).append("\n").toString();
				
		}
    
    
    default String getJSON(Coffee coffee) {
    	return new StringBuffer().append("{").append("\"Name\":").append("\"").append(coffee.getname())
    	.append("\"").append(",").append("\"BrandName\":").append(coffee.getBrandname()).append(",").append("\"Price\":")
    	.append("\"").append(coffee.getPrice()).append("\"").append(",").append("\"orderdate\":").append("\"")
    	.append(coffee.getDate()).append("\"").append(",").append("\"Order Date\":")
    	.append(coffee.getType()).append("}").append("\n").toString();



    	}
    void download() throws IOException, Exception;
    void downloadJSON() throws Exception;
	void delete(String name1);
	
    }


	



