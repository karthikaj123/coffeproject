package main;
import java.time.LocalDate;	
import java.util.Objects;


public class Coffee implements Comparable<Coffee> {

//	Declaring variables
	private String name,brandName;
	private int price=0;
	private LocalDate orderdate;
	private CoffeeType type;
	

	public Coffee(String name,String brandName,int price,LocalDate date,CoffeeType type) {
		this.name=name;
		this.brandName=brandName;
		this.price=price;
		this.orderdate=date;
		this.type=type;
	}
	

	public Coffee() {
		// TODO Auto-generated constructor stub
	}


	//	generating the getters and setters 
	public String getname() {
		return name;
	}
	public void setname(String Name) {
		this.name = Name;
	}
	public String getBrandname() {
		return brandName;
	}
	public void setBrandname(String brandName) {
		this.brandName = brandName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public LocalDate getDate() {
		return orderdate;
		
	}

	public void setDate(LocalDate date) {
		this.orderdate = date;
	}

	public CoffeeType getType() {
		return type;
	}

	public void setType(CoffeeType type) {
		this.type = type;
	}
	//toString method and StringBuffer/StringBuilder method
	public String toString()
	{	
		return new StringBuffer().append(" Name:").append(name).append("\n").append("Brand Name:").append(brandName).append("\n").append("Price:").append(price).append("\n").append("Date:").append(orderdate).append("Type:").append(type).append("\n").toString();
//		return new StringBuilder().append(Name).append("\n").append(brandName).append("\n").append(price).append("\n").append(Date).toString();
		
	}
		@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coffee other = (Coffee) obj;
		return Objects.equals(brandName, other.brandName) && Objects.equals(orderdate, other.orderdate)
				&& Objects.equals(name, other.name) && price == other.price && type == other.type;
	}


	
	@Override
	public int compareTo(Coffee coffee) {
		// TODO Auto-generated method stub
		if(price<coffee.price)
			return -1;
		else if(price>coffee.price)
			return 1;
		else
			return 0;
	}
	
	
}



