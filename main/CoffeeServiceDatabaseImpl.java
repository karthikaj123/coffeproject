package main;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class CoffeeServiceDatabaseImpl implements CoffeeService {
	String driver, url, user, password;
	

	public CoffeeServiceDatabaseImpl(String driver, String url, String user, String password) {

		this.driver = driver;
		this.url = url;
		this.user = user;
		this.password = password;

	}

	private Connection getConnection() throws SQLException, ClassNotFoundException {

		Class.forName(driver);
		return DriverManager.getConnection(url, user, password);
	}
	
	private Coffee getCoffee(ResultSet resultset) throws Exception {
		
		Coffee coffee = new Coffee();
		coffee.setname(resultset.getString("name"));
		coffee.setBrandname(resultset.getString("brand_name"));
		coffee.setPrice(resultset.getInt("price"));
		coffee.setDate(resultset.getDate("order_date").toLocalDate());
		coffee.setType(CoffeeType.valueOf(resultset.getString("type")));
		return  coffee;
		
		
	}

	@Override
	public void add(Coffee coffee) {
		
		try {
			Connection connection;

			connection = getConnection();
			String query = "INSERT INTO coffee(name,brand_name,price,order_date,type) VALUES(?,?,?,?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, coffee.getname());
			preparedStatement.setString(2, coffee.getBrandname());
			preparedStatement.setInt(3, coffee.getPrice());
			preparedStatement.setDate(4, Date.valueOf(coffee.getDate()));
			preparedStatement.setString(5, coffee.getType().toString());

			preparedStatement.executeUpdate();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
			
		}

		System.out.println("Successfully inserted");

	}

	@Override
	public void displayElements() throws Exception {
		
		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from coffee");
			ResultSet resultSet = prestatement.executeQuery();
			while (resultSet.next()) {
				Coffee coffee = getCoffee(resultSet);
				System.out.println(coffee);

			}
			resultSet.close();
			connection.close();

		} catch (ClassNotFoundException|SQLException e) {
			
			e.printStackTrace();
		} 
	}
	
	
	@Override
	public void search(String name1) throws Exception {
		try {
			Connection connection ;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM coffee where name=?");
			preparedStatement.setString(1, name1);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())
				
			
				{
					Coffee coffee=getCoffee(resultSet);
					System.out.println(coffee) ;
					}
			
			connection.close();
		

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void sort() throws Exception {

		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from coffee order by price asc");
			ResultSet resultSet = prestatement.executeQuery();
			while (resultSet.next()) {
				Coffee coffee = getCoffee(resultSet);
				System.out.println(coffee);

			}
			resultSet.close();
			connection.close();

		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

	}

	@Override
	public List<Coffee> search(LocalDate date) throws Exception {
		List<Coffee> CoffeeList=new ArrayList<>();
		try {
			Connection connection = getConnection();
			connection = getConnection();

			PreparedStatement preparedStatement = connection
					.prepareStatement("SELECT * FROM coffee where order_date=?;");
			preparedStatement.setDate(1, Date.valueOf(date));
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Coffee coffee = getCoffee(resultSet);
				System.out.println(coffee);
			}
			connection.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return CoffeeList;
	}
	

	@Override
	public void download() throws Exception {
		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from coffee");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("CoffeeDatabaseDetails.csv");
			while (resultSet.next()) {
				
				String data =convertTocsv(getCoffee(resultSet));
				file.write(data);
			}
			file.close();
		} catch (ClassNotFoundException |  SQLException e) {
			
			e.printStackTrace();
		} 
	}
	

	

	@Override
	public void downloadJSON() throws Exception {
		
		Connection connection;

		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from coffee");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("CoffeeDatabaseDetails.json");
			StringJoiner join = new StringJoiner(",", "[", "]");
			while (resultSet.next()) {
				String data = join.add(getJSON(getCoffee(resultSet))).toString();
				file.write(data);

			}
			file.close();
			connection.close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	

	@Override
	public void delete(String name1) {
		try {
			Connection connection =getConnection();
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM coffee where name=?");
			preparedStatement.setString(1, name1);
			preparedStatement.executeUpdate();
			System.out.println("...Data deleted from the database successfully...");
			connection.close();
			

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

			
			
		}
	}


