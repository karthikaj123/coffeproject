package main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class CoffeeServiceArrayImpl implements CoffeeService {
	private Coffee[] coffees;
//	private Player []players=new Player[100];
	private int j = 0;

	public CoffeeServiceArrayImpl() {
		coffees = new Coffee[100];
	}

	public CoffeeServiceArrayImpl(int size) {
		coffees = new Coffee[size];
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	/**
	 * 
	 * 
	 * @param
	 */
	public void add(Coffee coffee) {
		boolean flag = true;
		if (j >= coffees.length)
			System.out.println("Data Limit exceeded");
		else if (flag == true) {
			for (int i = 0; i < j; i++) {
				if (coffees[i].equals(coffee)) {
					System.out.println("Duplicate Data");
					flag = false;
				}
			}
		}
		if (flag == true) {
			coffees[j] = coffee;
			j++;
		}
	}

	/**
	 * The method will display coffees
	 */
	public void displayElements() {
		if (j == 0) {
			System.out.println("No data To display");
		} else
			for (int i = 0; i < j; i++) {
				System.out.println(coffees[i]);
			}
	}

	/**
	 * The method will search for coffees
	 * 
	 * @param name
	 */
	public void search(String name) {
		boolean flag = false;
		for (int i = 0; i < j; i++) {
			if (coffees[i].getname().equalsIgnoreCase(name)) {
				System.out.println(coffees[i]);
				flag = true;
			}
		}
		if (flag == false)
			System.out.println("No data To display");
	}

	/**
	 * 1 The method will sort coffees
	 */
	public void sort() {
		Arrays.sort(coffees, 0, j);
		displayElements();
	}

	/**
	 * The method will Search Using Date funtion
	 * 
	 * @param dateOfBirth
	 */
	public List<Coffee> search(LocalDate date) {
		List<Coffee> coffeeList = new ArrayList<>();

		for (int i = 0; i < j; i++) {
			if (coffees[i].getDate().isAfter(date)) {
				coffeeList.add((coffees[i]));

			}
		}
		return coffeeList;
	}

	/**
	 * The method will download data to a csv format file
	 * 
	 */
	public void download() throws Exception {

		FileWriter file = new FileWriter("download.csv");
		for (int i = 0; i < j; i++) {
			String data = convertTocsv(coffees[i]);
			file.write(data);
		}
		file.close();
	}

	/**
	 * The method will download coffees data to a json format file
	 * 
	 */
	public void downloadJSON() throws Exception {
		String data;
		StringJoiner join = new StringJoiner(",", "[", "]");
		FileWriter fileWriter = new FileWriter("DonloadArray.json");
		for (int i = 0; i < j; i++) {
			data = getJSON(coffees[i]);
			join.add(data);
		}
		fileWriter.write(join.toString());
		fileWriter.close();
	}

	@Override
	public void delete(String name) {
		int k = 0;
		for (int i = 0; i < j; i++) {
			if (!coffees[i].getname().equals(name)) {
				coffees[k] = coffees[i];
				k++;
				System.out.println("deleted\n");
			}
		}
		j = k;
	}

}