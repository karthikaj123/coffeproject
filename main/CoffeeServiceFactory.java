package main;

import java.util.Properties;

public abstract class CoffeeServiceFactory {

	public static CoffeeService getCoffeeService(String type, Properties properties) {
		
		CoffeeService service = null;

		switch (type) {

		case "Array":
			service = new CoffeeServiceArrayImpl();
			break;

		case "ArrayList":

			service = new CoffeeServiceArrayListImpl();
			break;

		case "DataBase":
			String driver = properties.getProperty("driver");
			String url = properties.getProperty("url");
			String user = properties.getProperty("user");
			String password = properties.getProperty("password");
			service = new CoffeeServiceDatabaseImpl(driver, url, user, password);
		

		}
		return service;

	}
}
