/**
 * 
 */
package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import main.Coffee;
import main.CoffeeServiceArrayImpl;
import main.CoffeeServiceArrayListImpl;
import main.CoffeeServiceDatabaseImpl;
import main.CoffeeType;

/**
 * @author Karthika J
 *
 */
class TestCase {

	CoffeeServiceArrayImpl coffeeServiceArrayImpl = new CoffeeServiceArrayImpl();

	@Test
	public void test() {

		coffeeServiceArrayImpl
				.add(new Coffee("ColdCoffee", "Bru", 18, LocalDate.parse("1988-11-05"), CoffeeType.valueOf("COLD")));
		assertEquals(1, coffeeServiceArrayImpl.getJ());
	}

	@Test
	public void test1() {

		coffeeServiceArrayImpl.displayElements();
		assertEquals(1, coffeeServiceArrayImpl.getJ());
	}
	
	
//
//	@Test
//	public void test2() {
//
//		coffeeServiceArrayImpl.delete("Bru");
//		assertEquals(0, coffeeServiceArrayImpl.getJ());
//	}

}
